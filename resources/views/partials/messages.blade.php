@if (session()->has('success'))
    <div class="col-12">
        <div class="alert alert-success">
            <p>{{ session('success') }}</p>
        </div>
    </div>
@endif

@if (session()->has('error'))
    <div class="col-12">
        <div class="alert alert-warning">
            <p>{{ session('error') }}</p>
        </div>
    </div>
@endif
