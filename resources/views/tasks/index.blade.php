@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <h2>Tareas</h2>
        </div>
    </div>
    <div class="row">
        @forelse ($tasks as $task)
        <div class="col-12 mt-4">
            <article class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ $task->name }}</h4>
                    <span id="status" class="badge {{ $task->completed? "badge-success": "badge-warning"}}"> 
                        {{ $task->completed? "Terminada": "Pendiente"}}
                    </span>
                    <p class="card-text">{{ $task->description }}</p>
                </div>
                <div class="card-footer">
                    <form action="{{ route('tasks.complete', $task->id) }}" method="POST">
                        @csrf
                        <button type="submit" 
                                id="btn-mark-as-completed"
                                class="btn btn-success">Terminar</button>
                    </form>
                </div>
            </article>
        </div>
        @empty
        <div class="col-12">
            <div class="alert alert-warning">
                <h5>No tienes tareas!</h5>
            </div>
        </div>
        @endforelse
    </div>
@endsection