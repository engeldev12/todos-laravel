<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

use App\Models\Task;

class ShowListTaskTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testDeberiaVerElListadoDeLasTareasCreadas()
    {
        Task::factory()->create([
            "name" => "Transferir",
            "description" => "Una descripción"
        ]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Transferir')
                    ->assertSee('Pendiente')
                    ->assertSee('Una descripción');
        });
    }

    public function testDeberiaVerUnMensajeCuandoNoHayaTareas()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('No tienes tareas!');
        });
    }
}
