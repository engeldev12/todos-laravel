<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

use App\Models\Task;

class MarkTaskAsCompletedTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testPuedoMarcarUnaTareaPendienteComoTerminada()
    {
        Task::factory()->create([
            "name" => "Crear un microservicio",
            "description" => "El microservicio se creará en NodeJs",
        ]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Crear un microservicio')
                    ->click('#btn-mark-as-completed')
                    ->assertSee('La tarea "Crear un microservicio" ha sido terminada con éxito!')
                    ->assertSeeIn('#status', "Terminada");
        });
    }

    public function testNoPuedoMarcarDosVecesUnaTareaComoTerminadaDaError()
    {
        Task::factory()->create([
            "name" => "Crear un microservicio",
            "description" => "El microservicio se creará en NodeJs",
            "completed" => true,
        ]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Crear un microservicio')
                    ->assertSeeIn("#status", "Terminada")
                    ->click('#btn-mark-as-completed')
                    ->assertSee('Esta tarea "Crear un microservicio" ya fue terminada.');
        });
    }
}
