<?php

namespace Tests\Unit;

use Tests\TestCase;

use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Task;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    private Task $task;

    protected function setUp(): void
    {
        parent::setUp();
    
        $this->task = Task::factory()->create([
            "name" => "Programar",
            "description" => "Una descripción"
        ]);
    }
    
    public function testPuedoAgregarUnaTarea()
    {
        $this->assertEquals(1, Task::count());
    }

    public function testPuedoMarcarUnTareaComoTerminada()
    {
        $this->task->markAsCompleted();

        $this->assertTrue($this->task->completed);
    }
}
