<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Task;

class TasksTest extends TestCase
{
    use RefreshDatabase;

    public function testDeboPoderAccederALaPaginaPrincipal()
    {
        Task::factory()->create([
            "name" => "Programar",
            "description" => "Una descripción"
        ]);

        $response = $this->get('/');

        $response->assertStatus(200)
            ->assertSee("Programar");
    }
}
