<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Task;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Task::factory()->create([
            "name" => "Programar",
            "description" => "Una descripción"
        ]);

        Task::factory()->create([
            "name" => "Comer",
            "description" => "Una descripción"
        ]);

        Task::factory()->create([
            "name" => "Transferir",
            "description" => "Una descripción"
        ]);

    }
}
