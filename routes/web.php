<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TasksController;

Route::get('/', [TasksController::class, 'index'])
    ->name('tasks.index');

Route::post('/terminar/{task}', [TasksController::class, 'complete'])
    ->name('tasks.complete');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
