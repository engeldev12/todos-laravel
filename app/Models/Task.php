<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "description",
    ];
    
    public function markAsCompleted()
    {
        if ($this->completed) {
            return;
        }
        $this->completed = true;
        $this->save();
    }
}
