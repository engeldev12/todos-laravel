<?php

namespace App\Http\Controllers;

use App\Models\Task;

class TasksController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return view('tasks.index', [
            "tasks" => $tasks
        ]);
    }

    public function complete(Task $task)
    {
        if ($task->completed) {
            return redirect()->route('tasks.index')->with('error',
            "Esta tarea \"$task->name\" ya fue terminada."
            );
        }

        $task->markAsCompleted();
        return redirect()->route('tasks.index')->with('success',
            "La tarea \"$task->name\" ha sido terminada con éxito!"
        );
    }
}
